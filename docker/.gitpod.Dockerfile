FROM gitpod/workspace-full

# Install custom tools, runtime, etc.
RUN sudo apt-get update && sudo apt-get -yq dist-upgrade \
 && sudo apt-get install -yq --no-install-recommends \
    rsync \
    texlive-latex-recommended \
    texlive-fonts-recommended \
    texlive-lang-french \
    zip \
    spim \
    clang \
    time \
    file \
    tree \
    cmake \
 && sudo apt-get clean \
 && sudo rm -rf /var/lib/apt/lists/*
