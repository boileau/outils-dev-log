---
title: Manual
parent: home
---

## Installer des dépendances

### Python

Anaconda est nécessaire avec les dépéndances suivantes :

```bash
conda create -f docker/environment.yml
python3 -m bash_kernel.install
```

### Utilitaires

- `spim` (pour exécuter directement le code assembleur)
- `cmake 3.12+`

## Exécuter

```bash
jupyter-notebook
```

## Produire les supports

Depuis la racine du projet :

```bash
git clone --recurse-submodules https://gitlab.math.unistra.fr/boileau/nbcourse.git
pip install --user -r nbcourse/requirements.txt
python -m nbcourse.nbcourse
```

Le résultat se retrouve dans le répertoire `build` :

- les notebooks en version html linéaire et les slides Reveal.js
- une archive par notebook avec le matériel des TP
- une archive complète
- une compilation de l'ensemble des chapitres au sein d'un même pdf
- le menu `index.html` de la page d'accueil

## Publier

GitLab-ci publie le contenu à l'adresse indiquée sur [cette page](https://gitlab.math.unistra.fr/boileau/outils-dev-log/pages) grâce au fichier [.gitlab-ci.yml](.gitlab-ci.yml).