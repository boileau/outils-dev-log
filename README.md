[![Gitpod ready-to-code](https://img.shields.io/badge/Gitpod-ready--to--code-blue?logo=gitpod)](https://gitpod.io/#https://gitlab.math.unistra.fr/boileau/outils-dev-log)

## Installer des dépendances

### Python

Python3 est nécessaire avec les dépéndances suivantes :

```bash
pip install -r docker/gitpod-requirements.txt
python3 -m bash_kernel.install
```

### Utilitaires

- `spim` (pour exécuter directement le code assembleur)
- `cmake 3.12+`

## Exécuter

```bash
jupyter-notebook
```

## Produire les supports

### Installer [nbcourse](https://pypi.org/project/nbcourse/)

```bash
pip install nbcourse
```

### Construire les supports

Depuis la racine du projet :

```bash
nbcourse
```

Les supports se retrouvent dans le fichier `build/index.html`.

## Publier

GitLab-ci publie le contenu à l'adresse indiquée sur [cette page](https://gitlab.math.unistra.fr/boileau/outils-dev-log/pages) grâce au fichier [.gitlab-ci.yml](.gitlab-ci.yml).