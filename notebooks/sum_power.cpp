#include <iostream>

double powern(double d, unsigned n) {
  double x = 1.0;
  unsigned j;
  for (j = 1; j <= n; j++)
    x *= d;
  return x;
}

int main() {
  double sum = 0.0;
  unsigned i;
  for (i = 1; i <= 200000000; i++) {
      sum += powern (i, i % 5);
    }
  std::cout<<"sum = "<<sum<<std::endl;
  return 0;
}
