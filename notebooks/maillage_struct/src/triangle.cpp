#include <iostream>
#include <cmath>

#include "triangle.hpp"

using namespace std;

triangle::triangle() : polygone(3) {
}

triangle::triangle(point *p) : polygone(3, p) {
    set_h();
}

void triangle::set_h() {
    if (abs((m_sommets[1].get_x() - m_sommets[0].get_x()) * (m_sommets[2].get_y() - m_sommets[1].get_y()) - (m_sommets[1].get_y() - m_sommets[0].get_y()) * (m_sommets[2].get_x() - m_sommets[1].get_x())) > 1e-8) {
        segment m1 = m_segments[0].mediatrice();
        segment m2 = m_segments[1].mediatrice();
        point inters = m1.intersection(m2);
        point v = inters - m_sommets[0];
        m_h = v.norme() * 2;
    } else {
        m_h = 0;
    }
}

double triangle::perimetre() {
    double res = m_segments[0].longueur() + m_segments[1].longueur() + m_segments[2].longueur();
    return res;
}

void triangle::divise_triangle(triangle & t1, triangle & t2) {
    point mil = m_segments[1].milieu();
    point liste_points[3] = {m_sommets[0], m_sommets[1], mil};
    t1 = triangle(liste_points);
    liste_points[0] = m_sommets[0];
    liste_points[1] = mil;
    liste_points[2] = m_sommets[2];
    t2 = triangle(liste_points);
}

int triangle::testu_1() { // Test d'aire et barycentre
    int res = 0;
    point liste_points[] = {point(1,2), point(4,2), point(1,6)};
    triangle t = triangle(liste_points);
    if (t.aire() - 6 > 1e-8) {
        res = 1;
    }
    point p = t.barycentre();
    if (abs(p.get_x() - 2) > 1e-8) {
        res = 1;
    }
    if (abs(p.get_y() - 10./3) > 1e-8) {
        res = 1;
    }
    return res;
}

int triangle::testu_2() { // Test triangle divisé
    int res = 0;
    point liste_points[] = {point(-2, -1), point(2, 3), point(-1, 4)};
    triangle t = triangle(liste_points);
    triangle t1, t2;
    t.divise_triangle(t1, t2);
    point mil = (t.m_segments[1]).milieu();
    if ((t1.m_sommets[0] != t.m_sommets[0]) || (t1.m_sommets[1] != t.m_sommets[1]) || (t1.m_sommets[2] != mil)) {
        res = 1;
    }
    if ((t2.m_sommets[0] != t.m_sommets[0]) || (t2.m_sommets[1] != mil) || (t2.m_sommets[2] != t.m_sommets[2])) {
        res = 1;
    }
    return res;
}

int triangle::testu_3() { // Test de set_h
    int res = 0;
    point p1 = point(0, 0);
    point p2 = point(1, 0);
    point p3 = point(0, 1);
    point liste_points[] = {p1, p2, p3};
    double h_th = sqrt(2);
    triangle t = triangle(liste_points);
    if (abs(h_th - t.get_h()) > 1e-8) {
        res = 1;
    }
    return res;
}

int triangle::all_testsu() {
    int c = 0, t = 0; 
    triangle s;
    c = s.testu_1(); t += c;
    c = s.testu_2(); t += c;
    c = s.testu_3(); t += c;
    cout << " Tests unitaires réussis, triangle : " << 3-t << "/3" << endl;
    return 0;
}

