#ifndef POINT_HPP
#define POINT_HPP

#include <iostream>

class point {
    private:
        double m_x;
        double m_y;
    public:
        point();
        point(double abs, double ord);
        point(const point & p);
        ~point();
        double get_x();
        double get_y();
        void set_x(double abs);
        void set_y(double ord);
        point operator +(point p);
        point operator *(double k);
        point operator -(point p);
        point & operator =(const point & p);
        double operator ,(point p);
        double operator ^(point p);
        double norme();
        bool operator <(point p);
        bool operator >(point p);
        bool operator ==(point p);
        bool operator !=(point p);
        friend std::ostream & operator <<(std::ostream & out, const point & p);

        int testu_1();
        int testu_2();
        int testu_3();
        int all_testsu();
};

#endif
