#ifndef POLYGONE_HPP
#define POLYGONE_HPP

#include "point.hpp"
#include "segment.hpp"

class polygone {
    protected:
	int m_n;
        point *m_sommets;
        segment *m_segments;
        double m_h; // Longueur caractéristique
    public:
        polygone();
        polygone(int n);
        polygone(int n, point *p);
        polygone(const polygone &poly);
        ~polygone();
        polygone & operator =(const polygone &poly);
	int get_nb_sommets();
        point getPoint(int i);
        segment getSegment(int i);
        double get_h();
        void setPoint(int i, point p);
        virtual void set_h();
        double aire();
        point barycentre();
        bool contient_point(point p);
        int testu_1();
        int testu_2();
        int testu_3();
        int all_testsu();
};

#endif
