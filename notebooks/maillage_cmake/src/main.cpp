#include <iostream>

#include "quadrangle.hpp"
#include "maillage.hpp"

int main() {
    maillage<quadrangle, 4> m;
    m.all_testsu();
    return 0;
}
