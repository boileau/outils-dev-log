import matplotlib.pyplot as plt
import csv

filename = "paris.txt"

S = []
I = []
R = []

with open(filename, 'r') as csvfile:
    plots = csv.reader(csvfile, delimiter=',')
    for row in plots:
        S.append(float(row[0]))
        I.append(float(row[1]))
        R.append(float(row[2]))

x = [0.01 * i for i in range(len(S))]

plt.plot(x[:8000], S[:8000], label='S')
plt.plot(x[:8000], I[:8000], label='I')
plt.plot(x[:8000], R[:8000], label='R')
plt.title(filename)
plt.legend()
plt.show()
