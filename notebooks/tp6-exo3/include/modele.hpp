#ifndef MODELE_HPP
#define MODELE_HPP

#include <iostream>

#include <vector>

class Modele {
    protected:
        // Nombre de variables du modèle
        int m_n;
        // Conditions initiales du modèle
        std::vector<double> m_initial_value;
    public:
        // Renvoie la valeur de m_n
        int get_n();
        // Renvoie a valeur de m_initial_value
        std::vector<double> initial_value();
        // Copie le paramètre v dans m_initial_value
        void setInitialValue(std::vector<double> v);
        // Calcule le flux associé au vecteur en paramètre
	virtual std::vector<double> flux(std::vector<double>) = 0;
};

#endif
