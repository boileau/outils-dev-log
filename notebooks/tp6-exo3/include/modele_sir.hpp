#ifndef MODELE_SIR_HPP
#define MODELE_SIR_HPP

#include <vector>
#include "modele.hpp"

class ModeleSir : public Modele {
    private:
	double m_beta;
        double m_gamma;
	double m_nu;
	double m_mu;
        double m_g;
    public:
	ModeleSir();
	ModeleSir(double beta, double gamma, double nu, double mu, double g);
	~ModeleSir() {}
        double beta();
        double gamma();
        double nu();
        double mu();
        double g();
        virtual std::vector<double> flux(std::vector<double>);
};

#endif
