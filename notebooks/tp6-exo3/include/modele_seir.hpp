#ifndef MODELE_SEIR_HPP
#define MODELE_SEIR_HPP

#include <vector>
#include "modele.hpp"

class ModeleSeir : public Modele {
    private:
	double m_beta;
	double m_gamma;
	double m_alpha;
	double m_nu;
	double m_mu;
	double m_g;
    public:
	ModeleSeir();
	ModeleSeir(double beta, double gamma, double alpha, double nu, double mu, double g);
	~ModeleSeir() {}
	double g();
	std::vector<double> flux(std::vector<double>);
};

#endif
