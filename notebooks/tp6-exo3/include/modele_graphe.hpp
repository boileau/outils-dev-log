#ifndef MODELE_GRAPHE_HPP
#define MODELE_GRAPHE_HPP

#include <vector>
#include "modele.hpp"
#include "graphe.hpp"

// Le paramètre T correspond au type de modèle localement utilisé sur chaque noeud du graphe
template<class T>
class ModeleGraphe : public Modele {
    private:
        // Graphe sur lequel le modèle d'épidémie sera appliqué
        graphe<T> *m_graphe;
    public:
        ModeleGraphe();
        ModeleGraphe(graphe<T> *g);
        ~ModeleGraphe();
        graphe<T> *get_graphe();
        // Calcule les conditions initiales du modèle en fonction des conditions initiales 
        // du modèle associé à chaque noeud du graphe.
        void setInitialValue();
        // Calcule le flux associé au vecteur en paramètre
        virtual std::vector<double> flux(std::vector<double>);
};

template<class T>
ModeleGraphe<T>::ModeleGraphe() {
    m_graphe = NULL;
    m_n = 0;
}

template<class T> 
ModeleGraphe<T>::ModeleGraphe(graphe<T> *g) {
    m_graphe = g;
    T t;
    if (g != NULL) {
        m_n = g->get_nbnodes() * t.get_n();
    } else {
        m_n = 0;
    }
}

template<class T> 
ModeleGraphe<T>::~ModeleGraphe() {
}

template<class T> 
graphe<T> * ModeleGraphe<T>::get_graphe() {
    return m_graphe;
}

template<class T> 
void ModeleGraphe<T>::setInitialValue() {
    // création du vecteur contenant les conditions initiale :
    // On concatène les vecteurs de conditions initiales de chaque noeud ;
    // Si ils sont invalides, on met la valeur initiale à 0 par défaut.
    vector<double> res;
    for (int i=0; i<m_graphe->get_nbnodes(); i++) {
        T t;
        if (m_graphe->get_noeud(i)->get_val().initial_value().size() == t.get_n()) {
            for (int j=0; j<t.get_n(); j++) {
                res.push_back(m_graphe->get_noeud(i)->get_val().initial_value()[j]);
            }
        } else {
            for (int j=0; j<t.get_n(); j++) {
                res.push_back(0);
            }
        }
    }
    m_initial_value = res;
}

template<class T> 
vector<double> ModeleGraphe<T>::flux(vector<double> v) {
    vector<double> res(m_n);
    T t;
    // Traitement des termes de flux pour chaque noeud
    vector<double> flux_local;
    // Repère placé sur le premier élément correspondant au noeud considéré
    vector<double>::const_iterator debut_v_local;
    for (int i=0; i<m_graphe->get_nbnodes(); i++) {
        debut_v_local = v.begin() + i * t.get_n();
        // Flux associé au noeud i
        flux_local = m_graphe->get_noeud(i)->get_val().flux(vector<double>(debut_v_local, debut_v_local + t.get_n()));
        // Ajout du flux local dans le vecteur de flux global
        for (int j=0; j<t.get_n(); j++) {
            res[j + t.get_n() * i] += flux_local[j];
        }
        // Parcours des voisins du noeud
        // Numéro du noeud voisin considéré
        int n_voisin;
        // Poids associé à ce voisin
        double poids_voisin;
        // Ajout des termes de couplage
        for (int ind_voisin=0; ind_voisin<m_graphe->get_noeud(i)->get_voisins().size(); ind_voisin++) {
             n_voisin = m_graphe->get_noeud(i)->get_voisins()[ind_voisin].num();
             poids_voisin = m_graphe->get_noeud(i)->get_voisins()[ind_voisin].p();
             for (int j=0; j<t.get_n(); j++) {
                 res[j + t.get_n() * n_voisin] += m_graphe->get_noeud(i)->get_val().g() * poids_voisin * v[j + i * t.get_n()];
             }
        }
    }
    return res;
}

#endif
