#ifndef EULER_EX_HPP
#define EULER_EX_HPP

#include "solveur_temps.hpp"

class EulerEx: public SolveurTemps {
    public:
        EulerEx(): SolveurTemps() {}
        EulerEx(double dt): SolveurTemps(dt) {}
	// Calcule la solution pour un pas de temps supplémentaire
        void applique();
};

#endif
