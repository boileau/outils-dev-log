#ifndef SOLVEUR_TEMPS_HPP
#define SOLVEUR_TEMPS_HPP

#include <vector>

#include "modele.hpp"

class SolveurTemps {
    protected:
        // Pas de temps
	double m_dt;
        // Vecteur contenant les vecteurs solution à chaque pas de temps
	std::vector< std::vector<double> > m_variablesTemps;
        // Modèle d'épidémie utilisé
	Modele *m_modele;
    public:
        SolveurTemps();
        SolveurTemps(double dt);
        ~SolveurTemps();
        // Calcule la solution pour un pas de temps supplémentaire
        virtual void applique() = 0;
        // Etablit le modèle utilisé
	void setModele(Modele * m);
        // Calcule la valeur initiale du vecteur solution
        void setInitialValue();
        // Renvoie le temps correspondant au dernier vecteur de m_variablesTemps
        double getCurrentTime();
        // Renvoie le pas de temps
        double get_dt();
        // Renvoie la dernière solution calculée
        std::vector<double> getSolutionCurrentTime();
        // Renvoie les solutions pour chaque pas de temps
        std::vector< std::vector<double> > getVariablesTime();
        Modele* getModel();
};

#endif
