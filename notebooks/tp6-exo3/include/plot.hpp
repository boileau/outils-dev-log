#ifndef PLOT_HPP
#define PLOT_HPP

#include "solveur_temps.hpp"
#include "modele_graphe.hpp"
#include "matplotlibcpp.h"

namespace plt = matplotlibcpp;

template<class T>
class Plot {
    ModeleGraphe<T>* m_graphe;
    SolveurTemps* s_temps;
public:
    Plot() {
        m_graphe = NULL;
        s_temps = NULL;
    };
    Plot(ModeleGraphe<T>* modele_graphe, SolveurTemps* solveur_temps){
        m_graphe = modele_graphe;
        s_temps = solveur_temps;
   };
    ~Plot() {};

    void plotSolution(){
        int nb_noeuds = m_graphe->get_graphe()->get_nbnodes(); 
        int nvar = s_temps->getModel()->get_n() / nb_noeuds;  // SIR: nvar = 3 ; SEIR: nvar = 4

        // On boucle sur les noeuds pour afficher la solution finale et tracer l'évolution temporelle
        for (int inoeud=0; inoeud<nb_noeuds; inoeud++) {
            string noeud = m_graphe->get_graphe()->get_noeud(inoeud)->get_nom();
            cout << "Values for " << noeud << " : " << endl;
            for (int i=inoeud*nvar; i<(inoeud+1)*nvar; i++) {
                cout << s_temps->getSolutionCurrentTime()[i] << endl;
            }
            plotSolutionNoeud(nvar, inoeud, noeud);
        }
        // On trace l'évolution temporelle de la somme sur toutes les noeuds
        plotSolutionNoeud(nvar, -nb_noeuds, "Total");
    };

    void plotSolutionNoeud(const int nvar, const int inoeud, const string& nom) {

        std::vector< std::vector<double> > m_variablesTemps = s_temps->getVariablesTime();
        int nstep = m_variablesTemps.size();
        vector<double> t(nstep);
        vector<double> s(nstep);
        vector<double> e(nstep);
        vector<double> i(nstep);
        vector<double> r(nstep);

        if (inoeud >= 0) {
            // On cible la ville inoeud
            for(int k = 0; k < nstep; k++) {
                t[k] = s_temps->get_dt() * k;
                int ivar = inoeud*nvar;
                s[k] = m_variablesTemps[k][ivar];
                if (nvar == 4) { // cas SEIR
                    ivar++;
                    e[k] = m_variablesTemps[k][ivar];
                }
                ivar++;
                i[k] = m_variablesTemps[k][ivar];
                ivar++;
                r[k] = m_variablesTemps[k][ivar];
            }
        } else {
            // On fait la somme sur toutes les villes
            for(int k = 0; k < nstep; k++) {
                t[k] = s_temps->get_dt() * k;
                s[k] = 0.;
                e[k] = 0.;
                i[k] = 0.;
                r[k] = 0.;
                for(int j = 0; j < -inoeud; j++) {
                    int ivar = j*nvar;
                    s[k] += m_variablesTemps[k][ivar];
                    if (nvar == 4) { // cas SEIR
                        ivar++;
                        e[k] += m_variablesTemps[k][ivar];
                    }
                    ivar++;
                    i[k] += m_variablesTemps[k][ivar];
                    ivar++;
                    r[k] += m_variablesTemps[k][ivar];
                }
            }
        }

        map<string, string> keywords;  // utilisé seulement pour les labels

        keywords["label"] = "S";
        plt::plot(t, s, keywords);

        if (nvar == 4) { // cas SEIR
            keywords["label"] = "E";
            plt::plot(t, e, keywords);
        }

        keywords["label"] = "I";
        plt::plot(t, i, keywords);

        keywords["label"] = "R";
        plt::plot(t, r, keywords);

        plt::legend();  // On affiche la légende
        plt::title(nom);
        plt::xlabel("t");

        //plt::show();
        const string& filename = nom + ".png";
        plt::save(filename);
        cout << "Tracé sauvé dans " << filename << endl;

        plt::close();
    };

};

#endif
