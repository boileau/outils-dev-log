#ifndef RK2_HPP
#define RK2_HPP

#include "solveur_temps.hpp"

class Rk2: public SolveurTemps {
    public:
        Rk2(): SolveurTemps() {}
        Rk2(double dt): SolveurTemps(dt) {}
	void applique();
};

#endif
