#include "modele_sir.hpp"

using namespace std;

ModeleSir::ModeleSir() {
    m_n = 3;
    m_beta = 0;
    m_gamma = 0;
    m_nu = 0;
    m_mu = 0;
    m_g = 0;
}

ModeleSir::ModeleSir(double beta, double gamma, double nu, double mu, double g) {
    m_n = 3;
    m_beta = beta;
    m_gamma = gamma;
    m_nu = nu;
    m_mu = mu;
    m_g = g;
}

double ModeleSir::beta() {
    return m_beta;
}

double ModeleSir::gamma() {
    return m_gamma;
};

double ModeleSir::nu() {
    return m_nu;
}

double ModeleSir::mu() {
    return m_mu;
}
        
double ModeleSir::g() {
    return m_g;
}

vector<double> ModeleSir::flux(vector<double> v) {
    vector<double> res(m_n);
    res[0] = - m_beta * v[0] * v[1] - (m_nu + m_g) * v[0] + m_mu * (v[0] + v[1] + v[2]);
    res[1] = m_beta * v[0] * v[1] - m_gamma * v[1] - (m_nu + m_g) * v[1];
    res[2] = m_gamma * v[1] - (m_nu + m_g) * v[2];
    return res;
}
