#include "euler_ex.hpp"

using namespace std;

void EulerEx::applique() {
    // Solution au temps précédent
    vector<double> sol_n = getSolutionCurrentTime();
    // Solution calculée
    vector<double> sol_np1;
    // Flux évalué au temps t précédent
    vector<double> flux = m_modele->flux(sol_n);
    // Calcul de la solution à t + dt
    for (int i=0; i<sol_n.size(); i++) {
        sol_np1.push_back(sol_n[i] + m_dt * flux[i]);
    }
    // Ajout de la solution au vecteur des solutions
    m_variablesTemps.push_back(sol_np1);
}
