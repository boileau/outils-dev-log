#include <iostream>
#include <fstream>

#include "graphe.hpp"
#include "euler_ex.hpp"
#include "rk2.hpp"
#include "modele_sir.hpp"
#include "modele_graphe.hpp"
#include "plot.hpp"

using namespace std;

int main() {
    // Création du graphe de villes
    graphe<ModeleSir> france;

    noeud<ModeleSir> Paris("Paris");
    france.ajout_noeud(&Paris); // 0
    
    noeud<ModeleSir> Lille("Lille");
    france.ajout_noeud(&Lille); //1
    
    noeud<ModeleSir> Bordeaux("Bordeaux");
    france.ajout_noeud(&Bordeaux); //2 
    
    noeud<ModeleSir> Strasbourg("Strasbourg");
    france.ajout_noeud(&Strasbourg); //3
    
    noeud<ModeleSir> Nantes("Nantes");
    france.ajout_noeud(&Nantes); //4
    
    noeud<ModeleSir> Lyon("Lyon");
    france.ajout_noeud(&Lyon); //5
    
    noeud<ModeleSir> Marseille("Marseille");
    france.ajout_noeud(&Marseille); //6

    // Ordre des paramètres dans le constructeur du modèle : 
    // beta, gamma, nu, mu, g
    double g = 0.1;
    double beta = 0.7; //0.5
    double gamma = 0.1;
    ModeleSir modeleParis(beta, gamma, 0, 0, g);
    ModeleSir modeleLille(beta, gamma, 0, 0, g);
    ModeleSir modeleBordeaux(beta, gamma, 0, 0, g);
    ModeleSir modeleStrasbourg(beta, gamma, 0, 0, g);
    ModeleSir modeleNantes(beta, gamma, 0, 0, g);
    ModeleSir modeleLyon(beta, gamma, 0, 0, g);
    ModeleSir modeleMarseille(beta, gamma, 0, 0, g);
    // Création du vecteur contenant les valeurs initiales
    vector<double> v;
    v.push_back(0.34);
    v.push_back(0.1);
    v.push_back(0);
    modeleParis.setInitialValue(v);
    v[0] = 0.05;
    v[1] = 0;
    modeleLille.setInitialValue(v);
    v[0] = 0.05;
    modeleBordeaux.setInitialValue(v);
    v[0] = 0.1;
    modeleStrasbourg.setInitialValue(v);
    v[0] = 0.06;
    modeleNantes.setInitialValue(v);
    v[0] = 0.1;
    modeleLyon.setInitialValue(v);
    v[0] = 0.2;
    modeleMarseille.setInitialValue(v);

    // Définition des modèles pour chaque ville et
    // création des liens entre les villes
    Paris.ajout_val(modeleParis);
    Paris.ajout_voisin(Lille.num(),0.1);
    Paris.ajout_voisin(Strasbourg.num(),0.2);
    Paris.ajout_voisin(Bordeaux.num(),0.1);
    Paris.ajout_voisin(Nantes.num(),0.1);
    Paris.ajout_voisin(Lyon.num(),0.2);
    Paris.ajout_voisin(Marseille.num(),0.3);

    Lille.ajout_val(modeleLille);
    Lille.ajout_voisin(Paris.num(),0.8);
    Lille.ajout_voisin(Strasbourg.num(),0.2);

    Strasbourg.ajout_val(modeleStrasbourg);
    Strasbourg.ajout_voisin(Paris.num(),0.7);
    Strasbourg.ajout_voisin(Lille.num(),0.1);
    Strasbourg.ajout_voisin(Lyon.num(),0.2);

    Bordeaux.ajout_val(modeleBordeaux);
    Bordeaux.ajout_voisin(Paris.num(),1.0);

    Nantes.ajout_val(modeleNantes);
    Nantes.ajout_voisin(Paris.num(),1.0);

    Lyon.ajout_val(modeleLyon);
    Lyon.ajout_voisin(Paris.num(),0.7);
    Lyon.ajout_voisin(Marseille.num(),0.2);
    Lyon.ajout_voisin(Strasbourg.num(),0.1);

    Marseille.ajout_val(modeleMarseille);
    Marseille.ajout_voisin(Paris.num(),0.8);
    Marseille.ajout_voisin(Lyon.num(),0.2);

    // Création du modèle d'épidémie sur le graphe de villes
    ModeleGraphe<ModeleSir> modele_graphe(&france);
    // Calcul des conditions initiales
    modele_graphe.setInitialValue();

    // Initialisation du solveur en temps
    double dt = 1e-2;
    Rk2 solveur_temps(dt);
    solveur_temps.setModele(&modele_graphe);
    solveur_temps.setInitialValue();

    // Décommenter ce qui suit pour écrire les résultats dans un fichier
    // Attention à décommenter aussi la fermeture du fichier plus bas
    //ofstream myfile;
    //myfile.open ("paris.txt");
    double seuil_epidemique = 0.15;
    double t_max = 90.;
    // Compteur d'itérations
    int counter = 0;
    while (solveur_temps.getCurrentTime() < t_max) {
        solveur_temps.applique();
        
        // On vérifie tous les 100 pas de temps si une ville dépasse le seuil épidémique
        if (counter % 100 == 0) {
            vector<double> sol = solveur_temps.getSolutionCurrentTime();
            for (int ind = 0; ind < 7; ind++) {
                if (sol[1 + ind * 3] / (sol[ind * 3] + sol[1 + ind * 3] + sol[2 + ind * 3]) > seuil_epidemique) {
                    cout << "A t=" << solveur_temps.getCurrentTime() << ", la ville n°" << ind << " est en quarantaine" << endl;
                }
            }
        }
        counter++;
        
        // A décommenter pour écrire les résultats pour l'ensemble des villesdans un fichier 
        /*double S = 0, I = 0, R = 0;
        for (int i=0; i<7; i++) {
            S += solveur_temps.getSolutionCurrentTime()[i * 3];
            I += solveur_temps.getSolutionCurrentTime()[1 + i * 3];
            R += solveur_temps.getSolutionCurrentTime()[2 + i * 3];
        }
        myfile << S << "," << I << "," << R << endl;
        */
        // A décommenter pour écrire les résultats correspondants à une ville dans un fichier
        //myfile << solveur_temps.getSolutionCurrentTime()[0] << "," << solveur_temps.getSolutionCurrentTime()[1] << "," << solveur_temps.getSolutionCurrentTime()[2] << endl;
    }
    //myfile.close();

    cout << "Final time : " << solveur_temps.getCurrentTime() << endl;
    // On crée un objet plot et on trace la solution
    Plot<ModeleSir> plot(&modele_graphe, &solveur_temps);
    plot.plotSolution();
    
    return 0;
}
