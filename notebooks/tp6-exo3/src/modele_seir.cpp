#include "modele_seir.hpp"

using namespace std;

ModeleSeir::ModeleSeir() {
    m_n = 4;
    m_beta = 0;
    m_gamma = 0;
    m_alpha = 0;
    m_nu = 0;
    m_mu = 0;
    m_g = 0;
}

ModeleSeir::ModeleSeir(double beta, double gamma, double alpha, double nu, double mu, double g) {
    m_n = 4;
    m_beta = beta;
    m_gamma = gamma;
    m_alpha = alpha;
    m_nu = nu;
    m_mu = mu;
    m_g = g;
}

double ModeleSeir::g() {
    return m_g;
}

vector<double> ModeleSeir::flux(vector<double> v) {
    vector<double> res(m_n);
    res[0] = - m_beta * v[0] * v[1] - (m_nu + m_g) * v[0] + m_mu * (v[0] + v[2] + v[3]);
    res[1] = m_beta * v[0] * v[1] - m_alpha * v[1] - (m_g + m_nu) * v[1];
    res[2] = m_alpha * v[1] - m_gamma * v[2] - (m_nu + m_g) * v[2];
    res[3] = m_gamma * v[2] - (m_nu + m_g) * v[3];
    return res;
}
