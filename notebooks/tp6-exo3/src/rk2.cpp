#include "rk2.hpp"

using namespace std;

void Rk2::applique() {
    // Solution au temps précédent
    vector<double> sol_n = getSolutionCurrentTime();
    // Solution calculée
    vector<double> sol_np1;
    vector<double> sol_np2;
    // Flux évalué au temps t précédent
    vector<double> flux = m_modele->flux(sol_n);
    // Calcul de la solution à t + dt/2
    for (int i=0; i<sol_n.size(); i++) {
        sol_np1.push_back(sol_n[i] + 0.5 * m_dt * flux[i]);
    }
    flux = m_modele->flux(sol_np1);
    for (int i=0; i<sol_n.size(); i++) {
        sol_np2.push_back(sol_np1[i] + 0.5 * m_dt * flux[i]);
    }
    m_variablesTemps.push_back(sol_np2);
}

