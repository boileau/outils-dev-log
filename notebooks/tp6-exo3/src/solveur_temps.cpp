#include "solveur_temps.hpp"

using namespace std;

SolveurTemps::SolveurTemps() {
    m_dt = 0.001;
    m_modele = NULL;
    vector< vector<double> > v;
    m_variablesTemps = v;
}

SolveurTemps::SolveurTemps(double dt) {
    m_dt = dt;
    m_modele = NULL;
    vector< vector<double> > v;
    m_variablesTemps = v;
}

SolveurTemps::~SolveurTemps() {
}

void SolveurTemps::setModele(Modele *m) {
    m_modele = m;
}

Modele* SolveurTemps::getModel() {
    return m_modele;
}

void SolveurTemps::setInitialValue() {
    vector< vector<double> > v;
    if (m_modele == NULL) {
        cout << "Impossible d'initialiser le solveur en temps : aucun modèle établi" << endl;
    } else if (m_modele->initial_value().size() != m_modele->get_n()) {
        cout << "Impossible d'initialiser le solveur en temps : le modèle ne contient pas d e conditions initiales" << endl;
    } else {
        v.push_back(m_modele->initial_value());
    }
    m_variablesTemps = v;
}

double SolveurTemps::getCurrentTime() {
    return m_dt * (m_variablesTemps.size() - 1);
}

double SolveurTemps::get_dt() {
    return m_dt;
}

vector<double> SolveurTemps::getSolutionCurrentTime() {
    return m_variablesTemps.back();
}

vector< vector<double> > SolveurTemps::getVariablesTime() {
    return m_variablesTemps;
}
