#include "modele.hpp"

int Modele::get_n() {
    return m_n;
}

std::vector<double> Modele::initial_value() {
    return m_initial_value;
}

void Modele::setInitialValue(std::vector<double> v) {
    if (v.size() != m_n) {
        std::cout << "Impossible d'établir la valeur initiale : taille de vecteur incorrecte" << std::endl;
    } else {
        m_initial_value = v;
    }
}
        
