#include <cmath>

#include "point.hpp"
#include "catch.hpp"

using namespace std;

point::point() {
    m_x = 0; m_y = 0;
}

point::point(double abs, double ord) {
    m_x = abs; m_y = ord;
}

point::point(const point & p) {
    m_x = p.m_x; m_y = p.m_y;
}

point::~point() {
}

double point::get_x() {
    return m_x;
}

double point::get_y() {
    return m_y;
}

void point::set_x(double abs) {
    m_x = abs;
}

void point::set_y(double ord) {
    m_y = ord;
}

point point::operator +(point p) {
    point res;
    res.m_x = m_x + p.m_x;
    res.m_y = m_y + p.m_y;
    return res;
}

point point::operator *(double k) {
    point res;
    res.m_x = m_x * k;
    res.m_y = m_y * k;
    return res;
}

point point::operator -(point p) {
    point res;
    res.m_x = m_x - p.m_x;
    res.m_y = m_y - p.m_y;
    return res;
}

point & point::operator =(const point & p) {
    if (&p != this) {
        m_x = p.m_x;
        m_y = p.m_y;
    }
    return *this;
}

double point::operator ,(point p) {
    double res = p.m_x * m_x + p.m_y * m_y;
    return res;
}

double point::operator ^(point p) {
    double res;
    res = m_x * p.m_y - m_y * p.m_x;
    return res;
}

double point::norme() {
    double res = sqrt(m_x * m_x + m_y * m_y);
    return res;
}

bool point::operator <(point p) {
    bool res = false;
    if (this->norme() < p.norme()) {
        res = true;
    }
    return res;
}

bool point::operator >(point p) {
    bool res = false;
    if (this->norme() > p.norme()) {
        res = true;
    }
    return res;
}

bool point::operator ==(point p) {
    bool res = false;
    if ((abs(this->m_x - p.m_x) < 1e-8) && (abs(this->m_y - p.m_y) < 1e-8)) {
        res = true;
    }
    return res;
}

bool point::operator !=(point p) {
    bool res = false;
    if ((abs(this->m_x - p.m_x) > 1e-8) || (abs(this->m_y - p.m_y) > 1e-8)) {
        res = true;
    }
    return res;
}
        
std::ostream & operator <<(std::ostream & out, const point & p) {
    out << "(" << p.m_x << ", " << p.m_y << ")";
    return out;
}

// Test des constructeurs, setters, getters, opérateurs d'égalité,
// inégalité, affectation
TEST_CASE("Point base"){
    point p1 = point(3, -4);
    REQUIRE(p1.get_x() == Approx(3));
    REQUIRE(p1.get_y() == Approx(-4));

    point p;
    REQUIRE(p.get_x() == Approx(0));
    REQUIRE(p.get_y() == Approx(0));

    p.set_x(10);
    p.set_y(56);
    REQUIRE(p.get_x() == Approx(10));
    REQUIRE(p.get_y() == Approx(56));

    bool equal = (p == point(10, 56));
    REQUIRE(equal);

    p = p1;
    bool copy = (p == p1);
    REQUIRE(copy);
}

// Test des opérations
TEST_CASE("Point - opérations"){
    point p1 = point(1, -6);
    point p2 = point(-3, 4);

    bool addition = (p1 + p2 == point(-2, -2));
    REQUIRE(addition);

    bool soustraction = (p1 - p2 == point(4, -10));
    REQUIRE(soustraction);

    REQUIRE((p1, p2) == -27);

    REQUIRE((p1 ^ p2) == -14);

    REQUIRE(p1.norme() == Approx(sqrt(37)));

    double k = 0.5;
    bool mult = (p1 * k == point(0.5, -3));
    REQUIRE(mult);
}

// Test des comparaisons
TEST_CASE("Point - comparaisons"){
    point p = point(1, 2);
    point q = point(3, -7);

    bool greater = (p > q);
    REQUIRE_FALSE(greater);

    bool lower = (p < q);
    REQUIRE(lower);
}
