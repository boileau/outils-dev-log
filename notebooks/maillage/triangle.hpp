#ifndef TRIANGLE_HPP
#define TRIANGLE_HPP

#include "point.hpp"
#include "segment.hpp"
#include "polygone.hpp"

class triangle : public polygone {
    public:
        triangle();
        triangle(point *p);
        void set_h();
        double perimetre();
        void divise_triangle(triangle &t1, triangle &t2);
        int testu_1();
        int testu_2();
        int testu_3();
        int all_testsu();
};

#endif
