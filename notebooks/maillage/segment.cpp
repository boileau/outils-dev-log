#include <iostream>
#include <cmath>

#include "segment.hpp"

using namespace std;

segment::segment() {
   m_p1 = point();
   m_p2 = point(0, 1); 
}

segment::segment(point p) {
    m_p1 = point();
    m_p2 = p;
}

segment::segment(point p1, point p2) {
    m_p1 = p1;
    m_p2 = p2;
}

segment::~segment() {
}

point segment::get_p1() {
    return m_p1;
}

point segment::get_p2() {
    return m_p2;
}

void segment::set_p1(point p) {
    m_p1 = p;
}

void segment::set_p2(point p) {
    m_p2 = p;
}

bool segment::operator ==(segment s) {
    bool res = false;
    if (((m_p1 == s.m_p1) && (m_p2 == s.m_p2)) || ((m_p1 == s.m_p2) && (m_p2 == s.m_p1))) {
        res = true;
    }
    return res;
}

double segment::longueur() {
    double res = sqrt((m_p2.get_x() - m_p1.get_x()) * (m_p2.get_x() - m_p1.get_x()) 
                    + (m_p2.get_y() - m_p1.get_y()) * (m_p2.get_y() - m_p1.get_y()));
    return res;
}

point segment::milieu() {
    point res;
    res.set_x((m_p1.get_x() + m_p2.get_x()) / 2);
    res.set_y((m_p1.get_y() + m_p2.get_y()) / 2);
    return res;
}

segment segment::mediatrice() {
    segment res;
    point p = point(1, 0);
    // Calcul du vecteur normal d'abscisse 1
    double y = (m_p1.get_x() - m_p2.get_x()) / (m_p2.get_y() - m_p1.get_y());
    p.set_y(y);
    // Normalisation du vecteur normal
    double norme_p = p.norme();
    p.set_x(p.get_x() / norme_p);
    p.set_y(p.get_y() / norme_p);
    res = segment(point(0, 0), p);
    // Translation du vecteur pour que l'origine soit le milieu du segment
    point milieu = this->milieu();
    res.m_p1.set_x(res.m_p1.get_x() + milieu.get_x());
    res.m_p1.set_y(res.m_p1.get_y() + milieu.get_y());
    res.m_p2.set_x(res.m_p2.get_x() + milieu.get_x());
    res.m_p2.set_y(res.m_p2.get_y() + milieu.get_y());
    return res;
}

point segment::intersection(segment s) {
    point res = point();
    double x1 = m_p1.get_x(), y1 = m_p1.get_y();
    double x2 = m_p2.get_x(), y2 = m_p2.get_y();
    double x3 = s.m_p1.get_x(), y3 = s.m_p1.get_y();
    double x4 = s.m_p2.get_x(), y4 = s.m_p2.get_y();
    // Cas où aucun segment n'est vertical
    if ((abs(x4 - x3) > 1e-8) && (abs(x2 - x1) > 1e-8)) {
        double a1 = (y4 - y3) / (x4 - x3);
        double a2 = (y2 - y1) / (x2 - x1);
        if (abs(a1 - a2) > 1e-8) {
            double b1 = y4 - a1 * x4;
            double b2 = y2 - a2 * x2;
            double x = (b2 - b1) / (a1 - a2);
            double y = a1 * x + b1;
            res = point(x, y);
        } else {
            cout << "Erreur : pas de point d'intersection entre les deux segments, ils sont parallèles" << endl;
        }
    // Cas où l'un des segments est vertical
    } else {
        if ((abs(x4 - x3) < 1e-8) && (abs(x2 - x1) < 1e-8)) {
            cout << "Erreur : pas de point d'intersection entre les deux segments, ils sont parallèles" << endl;
        } else if (abs(x4 - x3) < 1e-8) {
            double a2 = (y2 - y1) / (x2 - x1); 
            double b2 = y2 - a2 * x2;
            double x = x4;
            double y = a2 * x + b2;
            res = point(x, y);
        } else {
            double a1 = (y4 - y3) / (x4 - x3);
            double b1 = y4 - a1 * x4;
            double x = x2;
            double y = a1 * x + b1;
            res = point(x, y);
        }
    }
    return res;
}

int segment::testu_1() { // Test des constructeurs, getters, setters
    int res = 0;
    segment s;
    if ((s.m_p1 != point(0,0)) || (s.m_p2 != point(0,1))) {
        res = 1;
    }
    point p = point(7,4);
    s = segment(p);
    if ((s.m_p1 != point(0,0)) || (s.m_p2 != p)) {
        res = 1;
    }
    point p1 = point(2,-3);
    s = segment(p, p1);
    if ((s.m_p1 != p) || (s.m_p2 != p1)) {
        res = 1;
    }
    s = segment();
    s.set_p1(p);
    s.set_p2(p1);
    if ((s.get_p1() != p) || (s.get_p2() != p1)) {
        res = 1;
    }
    return res;
}

int segment::testu_2() { // Test de longueur et milieu
    int res = 0;
    point p1 = point(2, 1);
    point p2 = point(5, 5);
    segment s = segment(p1, p2);
    if (abs(s.longueur() - 5) > 1e-8) {
        res = 1;
    }
    if (s.milieu() != point(3.5, 3)) {
        res = 1;
    }

    segment s1 = segment(p2, p1);
    if (!(s1 == s)) {
        res = 1;
    }
    if (!(s == s)) {
        res = 1;
    }
    return res;
}

int segment::testu_3() { // Test de mediatrice
    int res = 0;
    point p1 = point(2, 1);
    point p2 = point(5, 5);
    segment s = segment(p1, p2);
    point p3 = point(3.5, 3);
    point p4 = point(4./5 + 3.5, -3./5 + 3);
    segment n = s.mediatrice();
    if ((n.m_p1 != p3) || (n.m_p2 != p4)) {
        res = 1;
    }
    return res;
}

int segment::testu_4() { // Test de l'intersection
    int res = 0;
    point p1 = point(-2, 0);
    point p2 = point(-1, -2);
    point p3 = point(1, -1);
    point p4 = point(3, 1);
    segment s1 = segment(p1, p2);
    segment s2 = segment(p3, p4);
    point inters = point(-2./3, -8./3);
    if (inters != s1.intersection(s2)) {
        res = 1;
    }
    point p5 = point(1, 4);
    s2.set_p2(p5);
    inters = point(1, -6);
    if (inters != s1.intersection(s2)) {
        res = 1;
    }
    return res;
}

int segment::all_testsu() {
    int c = 0, t = 0; 
    segment s;
    c = s.testu_1(); t += c;
    c = s.testu_2(); t += c;
    c = s.testu_3(); t += c;
    c = s.testu_4(); t += c;
    cout << " Tests unitaires réussis, segment : " << 4-t << "/4" << endl;
    return 0;
}

