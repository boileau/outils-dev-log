#ifndef QUADRANGLE_HPP
#define QUADRANGLE_HPP

#include "point.hpp"
#include "segment.hpp"
#include "triangle.hpp"

class quadrangle : public polygone {
    public:
        quadrangle();
        quadrangle(point *p);
        void set_h();
        void divise_triangles(triangle &t1, triangle &t2);
        int testu_1();
        int testu_2();
        int testu_3();
        int all_testsu();
};

#endif
