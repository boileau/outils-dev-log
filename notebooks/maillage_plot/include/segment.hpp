#ifndef SEGMENT_HPP
#define SEGMENT_HPP

#include "point.hpp"

class segment {
    private:
        point m_p1;
        point m_p2;
    public:
        segment();
        segment(point p);
        segment(point p1, point p2);
        ~segment();
        point get_p1();
        point get_p2();
        void set_p1(point p);
        void set_p2(point p);
        bool operator ==(segment s);
        double longueur();
        point milieu();
        segment mediatrice();
        point intersection(segment s);
        int testu_1();
        int testu_2();
        int testu_3();
        int testu_4();
        int all_testsu();
};

#endif
