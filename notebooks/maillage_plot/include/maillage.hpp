#ifndef MAILLAGE_H
#define MAILLAGE_H

#include <iostream>
#include <algorithm>

#include "point.hpp"
#include "triangle.hpp"
#include "quadrangle.hpp"
#include "polygone.hpp"

#include "matplotlibcpp.h"
#include <vector>

namespace plt = matplotlibcpp;

template<class T, int n> class maillage {
    private:
        int m_nb_noeuds;
        point *m_noeuds = NULL;
        int m_nb_mailles;
        T *m_mailles = NULL;
        int *m_ind_noeuds = NULL;
        int *m_voisins = NULL;
        double m_h;

	void swap(maillage & m);
        // Calcule le nombre de sommets communs entre les éléments d'indice i et j
        int nb_sommets_communs(int i, int j);
    public:
        maillage();
        maillage(const maillage & m);
        maillage(int nb_mailles, int nb_noeuds);
        ~maillage();
        int getNbNoeuds();
        point getNoeud(int i);
        int getNbMailles();
        T getMaille(int i);
        void set_h();
        void trace();
        maillage & operator =(const maillage & m);
        template<class U, int p>
        friend std::ostream & operator <<(std::ostream & out, const maillage<U,p> & m);
        void maillage_regulier(int N);
        void remplir_tableau_voisins();
        int testu_1();
        int testu_2();
        int testu_3();
        int testu_4();
        int all_testsu();
};

using namespace std;

// Constructeur par défaut
template<class T, int n> maillage<T, n>::maillage() {
    m_nb_noeuds = 0;
    m_noeuds = new point[0];
    m_nb_mailles = 0;
    m_mailles = new T[0];
    m_ind_noeuds = new int[0];
    m_voisins = new int[0];
}

// Constructeur par copie
template<class T, int n> maillage<T, n>::maillage(const maillage<T, n> & m) {
    m_nb_noeuds = m.m_nb_noeuds;
    m_noeuds = new point[m_nb_noeuds];
    for (int i=0; i<m_nb_noeuds; i++) {
        m_noeuds[i] = point(m.m_noeuds[i]);
    }
    m_nb_mailles = m.m_nb_mailles;
    m_mailles = new T[m_nb_mailles];
    for (int i=0; i<m_nb_mailles; i++) {
        m_mailles[i] = T(m.m_mailles[i]);
    }
    if (m_nb_mailles > 0) {
        int size_ind_noeuds = 0;
        for (int i=0; i<m_nb_mailles; i++) {
            size_ind_noeuds += m.m_mailles[i].get_nb_sommets();
        }
        m_ind_noeuds = new int[size_ind_noeuds];
        for (int i=0; i<size_ind_noeuds; i++) {
            m_ind_noeuds[i] = m.m_ind_noeuds[i];
        }
    } else {
        m_ind_noeuds = new int[0];
    }
    m_voisins = new int[m_nb_mailles * n];
    for (int i=0; i<m_nb_mailles * n; i++) {
        m_voisins[i] = m.m_voisins[i];
    }

    m_h = m.m_h;
}

// Constructeur à partir du nombre de mailles et du nombre de noeuds
template<class T, int n> maillage<T, n>::maillage(int nb_mailles, int nb_noeuds) {
    m_nb_noeuds = nb_noeuds;
    m_noeuds = new point[nb_noeuds];
    m_nb_mailles = nb_mailles;
    m_mailles = new T[nb_mailles];
    T t = T();
    m_ind_noeuds = new int[t.get_nb_sommets() * m_nb_mailles];
    m_voisins = new int[m_nb_mailles * n];
    for (int i=0; i<m_nb_mailles * n; i++) {
        m_voisins[i] = -1;
    }
    m_h = 0.;
}

// Spécialisation dans le cas du maillage mixte : m_ind_noeuds n'est pas alloué,
// Il faut le faire à la main lorsqu'on connaît le nombre de triangles et de
// quadrangles du maillage.
template<> maillage<polygone, 4>::maillage(int nb_mailles, int nb_noeuds) {
    m_nb_noeuds = nb_noeuds;
    m_noeuds = new point[nb_noeuds];
    m_nb_mailles = nb_mailles;
    m_mailles = new polygone[nb_mailles];
    m_voisins = new int[m_nb_mailles * 4];
    for (int i=0; i<m_nb_mailles * 4; i++) {
        m_voisins[i] = -1;
    }
    m_h = 0.;
}

template<class T, int n> maillage<T, n>::~maillage() {
    delete[] m_noeuds;
    delete[] m_mailles;
    delete[] m_ind_noeuds;
    delete[] m_voisins;
}

template <class T, int n> int maillage<T,n>::getNbNoeuds() {
    return m_nb_noeuds;
}

template<class T, int n> point maillage<T,n>::getNoeud(int i) {
    point p;
    if (i < m_nb_noeuds) {
        p = m_noeuds[i];
    } else {
        cout << "Erreur : indice de noeud incorrect" << endl;
    }
    return p;
}

template <class T, int n> int maillage<T,n>::getNbMailles() {
    return m_nb_mailles;
}

template<class T, int n> T maillage<T,n>::getMaille(int i) {
    T maille;
    if (i < m_nb_mailles) {
        maille = m_mailles[i];
    } else {
        cout << "Erreur : indice de maille incorrect" << endl;
    }
    return maille;
}

// Etablit h comme la plus petite des tailles caractéristiques du maillage
template<class T, int n> void maillage<T, n>::set_h() {
    if (m_nb_mailles > 0) {
        m_h = m_mailles[0].get_h();
        for (int i = 1; i < m_nb_mailles; i++) {
            if (m_h > m_mailles[i].get_h()) {
                m_h = m_mailles[i].get_h();
            }
        }
    } else {
        m_h = 0.;
    }
}

// Trace le maillage
template<class T, int n> void maillage<T, n>::trace() {
    T une_maille = getMaille(0);
    int nb_sommets = une_maille.get_nb_sommets();
    std::vector<double> x(m_nb_noeuds);
    std::vector<double> y(m_nb_noeuds);
    std::vector<double> xs(nb_sommets+1);
    std::vector<double> ys(nb_sommets+1);

    plt::figure();  // sinon: "segmentation fault" sur MacOS
    // Tracé des arêtes
    for(int i = 0; i < m_nb_mailles; i++) {
       T maille = getMaille(i);
       for(int j = 0; j < nb_sommets; j++) {
         xs[j] = maille.getPoint(j).get_x();
         ys[j] = maille.getPoint(j).get_y();
       };
       // dernier sommet = premier sommet
       xs[nb_sommets] = maille.getPoint(0).get_x();
       ys[nb_sommets] = maille.getPoint(0).get_y();
       plt::plot(xs, ys);
    }
    // Tracé des noeuds
    for(int i = 0; i < m_nb_noeuds; i++) {
       x[i] = m_noeuds[i].get_x();
       y[i] = m_noeuds[i].get_y();
    }
    // On veut des symboles circulaires
    plt::plot(x, y, "o");

    // Finalisation du tracé
    plt::title("Maillage");
    plt::axis("equal");
    plt::xlabel("x");
    plt::ylabel("y");
    plt::save("maillage.png");
    //plt::show();
    cout << "Tracé sauvé dans maillage.png" << endl;
}

template<class T, int n> maillage<T,n> & maillage<T,n>::operator =(const maillage<T,n> & m) {
    if (&m != this) {
        maillage<T,n> copie(m);
	    swap(copie);
    }
    return *this;
}

template<class T, int n> ostream & operator <<(ostream & out, const maillage<T,n> & m) {
    out << "Type de mailles : ";
    T t;
    if (t.get_nb_sommets() == 3) {
        out << "triangles" << endl;
    } else if (t.get_nb_sommets() == 4) {
        out << "quadrangles" << endl;
    } else {
        out << "indéterminé" << endl;
    }
    out << "Nombre max de voisins : " << n << endl;
    out << "Nombre de noeuds : " << m.m_nb_noeuds << endl;
    out << "Nombre de mailles : " << m.m_nb_mailles << endl;
    return out;
}

// Crée un maillage régulier de carrés
template<> void maillage<quadrangle, 4>::maillage_regulier(int N) {
    maillage<quadrangle, 4> m = maillage<quadrangle, 4>(N * N, (N + 1) * (N + 1));
    double delta = 1. / N;
    for (int i=0; i<N+1; i++) {
        for (int j=0; j<N+1; j++) {
	    m.m_noeuds[i * (N + 1) + j] = point(delta * i, delta * j);
	}
    }
    quadrangle q;
    for (int i=0; i<m.m_nb_mailles; i++) {
        m.m_ind_noeuds[0 + q.get_nb_sommets() * i] = i + i / N;
        m.m_ind_noeuds[3 + q.get_nb_sommets() * i] = i + 1 + i / N;
        m.m_ind_noeuds[1 + q.get_nb_sommets() * i] = i + N + 1 + i / N;
        m.m_ind_noeuds[2 + q.get_nb_sommets() * i] = i + N + 2 + i / N;
  
    }
    for (int k=0; k<N*N; k++) {
        q.setPoint(0, m.m_noeuds[m.m_ind_noeuds[0 + q.get_nb_sommets() * k]]);
        q.setPoint(1, m.m_noeuds[m.m_ind_noeuds[1 + q.get_nb_sommets() * k]]);
        q.setPoint(2, m.m_noeuds[m.m_ind_noeuds[2 + q.get_nb_sommets() * k]]);
        q.setPoint(3, m.m_noeuds[m.m_ind_noeuds[3 + q.get_nb_sommets() * k]]);
        m.m_mailles[k] = q;
    }
    m.set_h();
    (*this) = m; 
}

// Crée un maillage régulier de triangles
template<> void maillage<triangle, 3>::maillage_regulier(int N) {
    maillage<triangle, 3> m = maillage<triangle, 3>(2 * N * N, (N + 1) * (N + 1));
    double delta = 1. / N;
    for (int i=0; i<N+1; i++) {
        for (int j=0; j<N+1; j++) {
	    m.m_noeuds[i * (N + 1) + j] = point(delta * i, delta * j);
	}
    }
    triangle t;
    for (int k=0; k<N*N; k++) {
        m.m_ind_noeuds[0 + t.get_nb_sommets() * 2 * k] = k + k / N;
        m.m_ind_noeuds[1 + t.get_nb_sommets() * 2 * k] = k + 1 + k / N;
        m.m_ind_noeuds[2 + t.get_nb_sommets() * 2 * k] = k + N + 1 + k / N;
        m.m_ind_noeuds[0 + t.get_nb_sommets() * (2 * k + 1)] = k + 1 + k / N;
        m.m_ind_noeuds[1 + t.get_nb_sommets() * (2 * k + 1)] = k + N + 1 + k / N;
        m.m_ind_noeuds[2 + t.get_nb_sommets() * (2 * k + 1)] = k + N + 2 + k / N;
    }
    for (int k=0; k<N*N; k++) {
        t.setPoint(0, m.m_noeuds[m.m_ind_noeuds[0 + t.get_nb_sommets() * 2 * k]]);
        t.setPoint(1, m.m_noeuds[m.m_ind_noeuds[1 + t.get_nb_sommets() * 2 * k]]);
        t.setPoint(2, m.m_noeuds[m.m_ind_noeuds[2 + t.get_nb_sommets() * 2 * k]]);
        m.m_mailles[2 * k] = t;
        t.setPoint(0, m.m_noeuds[m.m_ind_noeuds[0 + t.get_nb_sommets() * (2 * k + 1)]]);
        t.setPoint(1, m.m_noeuds[m.m_ind_noeuds[1 + t.get_nb_sommets() * (2 * k + 1)]]);
        t.setPoint(2, m.m_noeuds[m.m_ind_noeuds[2 + t.get_nb_sommets() * (2 * k + 1)]]);
        m.m_mailles[2 * k + 1] = t;
    }
    m.set_h();
    (*this) = m; 
}

template <class T, int n> void maillage<T, n>::remplir_tableau_voisins() {
    delete [] m_voisins;
    m_voisins = new int[m_nb_mailles * n];
    for (int i=0; i<m_nb_mailles*n; i++) {
        m_voisins[i] = -1;
    }
    // Pour chaque maille, on parcourt toutes les autres mailles et on regarde
    // lesquelles sont voisines. On s'arrête si on atteint le nombre maximum
    // de voisins.
    int nb_voisins; int v;
    for (int m=0; m<m_nb_mailles; m++) {
        // nb_voisins représente le nombre de voisins que l'on a trouvé pour l'instant
        // pour l'élément considéré.
        nb_voisins = 0;
        // v est l'indice de l'élément dont on va tester si il est voisin ou non
        v = 0;
        while ((v < m_nb_mailles) && (nb_voisins < n)) {
            if (m!=v) {
                if (nb_sommets_communs(m, v) >= 2) {
                    m_voisins[nb_voisins + n * m] = v;
                    nb_voisins++;
                }
            }
            v++;
        }
    }
}

// Echange les membres de l'objet courant avec ceux de m.
// Fonction utilisée pour l'opérateur d'affectation.
template<class T, int n> void maillage<T, n>::swap(maillage<T, n> & m) {
	std::swap(m_nb_noeuds, m.m_nb_noeuds);
	std::swap(m_noeuds, m.m_noeuds);
	std::swap(m_nb_mailles, m.m_nb_mailles);
        std::swap(m_mailles, m.m_mailles);
	std::swap(m_ind_noeuds, m.m_ind_noeuds);
	std::swap(m_voisins, m.m_voisins);
	std::swap(m_h, m.m_h);
}

// Retourne le nombre de points communs entre l'élément d'indice i
// et l'élément d'indice j
template<class T, int n> int maillage<T,n>::nb_sommets_communs(int i, int j) {
    int res = 0;
    T t;
    int nb_sommets = t.get_nb_sommets();
    if ((i < m_nb_mailles) && (j < m_nb_mailles)) {
        for (int ii=0; ii<nb_sommets; ii++) {
            for (int jj=0; jj<nb_sommets; jj++) {
                if (m_ind_noeuds[ii + i * nb_sommets] == m_ind_noeuds[jj + j * nb_sommets]) {
                    res += 1;
                }
            }
        }
    }
    return res;
}


template<class T, int n> int maillage<T,n>::testu_1() { // Test de cout, constructeur, destructeur
    // Test du constructeur par nombre de points et de mailles
    int res = 0;
    maillage<triangle, 3> m;
    m = maillage(2, 4);
    if ((m.m_nb_mailles != 2) || (m.m_nb_noeuds != 4)) {
        res = 1;
    }
    cout << "Test cout sur maillage :\n" << m << endl;
    
    // Test du constructeur par copie 
    point p1 = point(0,0), p2 = point(1,0), p3 = point(1,1), p4 = point(0,1);
    point liste_points[] = {p1, p2, p3};
    triangle t1 = triangle(liste_points);
    liste_points[1] = p3;
    liste_points[2] = p4;
    triangle t2 = triangle(liste_points);
    m.m_noeuds[0] = p1;
    m.m_noeuds[1] = p2;
    m.m_noeuds[2] = p3;
    m.m_noeuds[3] = p4;
    m.m_mailles[0] = t1;
    m.m_mailles[1] = t2;
    
    maillage<triangle, 3> copie_m = maillage<triangle, 3>(m);
    if ((copie_m.m_nb_mailles != 2) || (copie_m.m_nb_noeuds != 4)) {
        res = 1;
    }
    if (copie_m.m_noeuds[2] != p3) {
        res = 1;
    }
    if (copie_m.m_mailles[1].getPoint(1) != p3) {
        res = 1;
    }

    // Test de l'opérateur d'affectation
    maillage<triangle, 3> m2;
    m2 = m;
    // On modifie m pour voir que la copie a bien été faite par valeurs.
    m = maillage<triangle, 3>();
    if ((m2.m_nb_mailles != 2) || (m2.m_nb_noeuds != 4)) {
        res = 1;
    }
    if (m2.m_noeuds[2] != p3) {
        res = 1;
    }
    if (m2.m_mailles[1].getPoint(1) != p3) {
        res = 1;
    }
    
    return res;
}

template<class T, int n> int maillage<T, n>::testu_2() { // Test de set_h et maillage_regulier
    int res = 0;
    int N = 3;
    maillage<quadrangle, 4> m;
    m.maillage_regulier(N);
    if ((m.m_nb_mailles != 9) || (m.m_nb_noeuds != 16)) {
        res = 1;
    }
    if (m.m_noeuds[1] != point(0,1./3)) {
        res = 1;
    }
    m.remplir_tableau_voisins();
    if ((m.m_voisins[0] != 1) || (m.m_voisins[1] != 3) || (m.m_voisins[2] != -1) || (m.m_voisins[3]) != -1) {
        res = 1;
    }
    return res;
}

template<class T, int n> int maillage<T, n>::testu_3() { // Test de set_h et voisins
    int res = 0;
    const int N = 3;
    maillage<triangle, 3> m;
    m.maillage_regulier(N);
    m.remplir_tableau_voisins();
    if ((m.m_voisins[0 + 3 * 3] != 2) || (m.m_voisins[1 + 3 * 3] != 4) || (m.m_voisins[2 + 3 * 3] != 8)) {
        res = 1;
    }
    return res;
}

template<> int maillage<polygone, 4>::testu_4() { // Test du maillage mixte
    int res = 0;
    maillage<polygone, 4> m(2, 5);
    point p1(0,0), p2(1,0), p3(1,1), p4(0,1), p5(2,0);
    point lq[] = {p1, p2, p3, p4};
    quadrangle q(lq);
    point lt[] = {p2, p3, p5};
    triangle t(lt);
    m.m_noeuds[0] = p1;
    m.m_noeuds[1] = p2;
    m.m_noeuds[2] = p3;
    m.m_noeuds[3] = p4;
    m.m_noeuds[4] = p5;
    m.m_mailles[0] = q;
    m.m_mailles[1] = t;
    delete[] m.m_ind_noeuds;
    m.m_ind_noeuds = new int[7];
    m.m_ind_noeuds[0] = 0;
    m.m_ind_noeuds[1] = 1;
    m.m_ind_noeuds[2] = 2;
    m.m_ind_noeuds[3] = 3;
    m.m_ind_noeuds[4] = 1;
    m.m_ind_noeuds[5] = 2;
    m.m_ind_noeuds[6] = 4; 
    return res;
}

template<class T, int n> int maillage<T,n>::all_testsu() {
    int c = 0, t = 0; 
    maillage<triangle, 3> m;
    maillage<quadrangle, 4> m_quad;
    maillage<polygone, 4> m_mixte;
    c = m.testu_1(); t += c;
    c = m_quad.testu_2(); t += c;
    c = m.testu_3(); t += c;
    c = m_mixte.testu_4(); t += c;
    cout << " Tests unitaires réussis, maillage : " << 4-t << "/4" << endl;
    return t;
}

#endif
