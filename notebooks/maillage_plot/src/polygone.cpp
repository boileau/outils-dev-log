#include <iostream>
#include <cmath>

#include "polygone.hpp"

using namespace std;

polygone::polygone() {
    m_n = 0;
    m_sommets = new point[0];
    m_segments = new segment[0]; 
    this->set_h();
}

polygone::polygone(int n) {
    m_n = n;
    m_sommets = new point[n];
    m_segments = new segment[n];
}

polygone::polygone(int n, point *p) {
    m_n = n;
    m_sommets = new point[n];
    m_segments = new segment[n];
    segment s;
    for (int i=0; i<n; i++) {
        m_sommets[i] = p[i];
        s.set_p1(p[i]);
        s.set_p2(p[(i+1) % n]);
        m_segments[i] = s; 
    }
    this->set_h(); 
}

polygone::polygone(const polygone &poly) {
    m_n = poly.m_n;
    m_sommets = new point[m_n];
    m_segments = new segment[m_n];
    for (int i=0; i<m_n; i++) {
        m_sommets[i] = poly.m_sommets[i];
        m_segments[i] = poly.m_segments[i];
    }
    m_h = poly.m_h;
}

polygone::~polygone() {
    delete [] m_sommets;
    delete [] m_segments;
}

polygone & polygone::operator =(const polygone &poly) {
    if (&poly != this) {
        m_n = poly.m_n;
        delete [] m_sommets;
        delete [] m_segments;
        m_sommets = new point[m_n];
        m_segments = new segment[m_n];
        for (int i=0; i<m_n; i++) {
            m_sommets[i] = poly.m_sommets[i];
            m_segments[i] = poly.m_segments[i];
        }
        m_h = poly.m_h;
    }
    return *this;
}

int polygone::get_nb_sommets() {
    return m_n;
}

point polygone::getPoint(int i) {
    return m_sommets[i];
}

segment polygone::getSegment(int i) {
    return m_segments[i];
}

double polygone::get_h() {
    return m_h;
}

void polygone::setPoint(int i, point p) {
    m_sommets[i] = p;
    if (i == 0) {
        m_segments[m_n - 1].set_p2(p);
    } else {
        m_segments[i-1].set_p2(p);
    }
    m_segments[i].set_p1(p);
    this->set_h();
}

// On n'a pas donné de définition générale pour la longueur
// caractéristique, elle sera redéfinie pour les classes
// héritées.
void polygone::set_h() {
    m_h = 0;
}

double polygone::aire() {
    double res = 0.;
    for (int i=0; i<m_n; i++) {
        res += (m_sommets[i]^m_sommets[(i+1) % m_n]);
    }
    res = 0.5 * abs(res);
    return res;
}

point polygone::barycentre() {
    point res;
    for (int i=0; i<m_n; i++) {
        res = res + (m_sommets[i] + m_sommets[(i+1) % m_n]) * (m_sommets[i]^m_sommets[(i+1) % m_n]);
    }
    res = res * (1. / 6 / this->aire());
    return res;    
}

bool polygone::contient_point(point p) {
    bool res = true;
    for (int i=1; i<m_n; i++) {
        if (((m_sommets[0] - p)^(m_sommets[1] - p)) * ((m_sommets[i] - p)^(m_sommets[(i+1) % m_n] - p)) < -1e-8) {
        res = false;
        }
    }
    return res;
}

int polygone::testu_1() { // Test aire, contient_point
    int res = 0;
    point p1 = point(0,0);
    point p2 = point(1,0);
    point p3 = point(0,1);
    point liste_points[] = {p1, p2, p3};
    polygone p = polygone(3,liste_points);
    if (abs(p.aire() - 0.5) > 1e-8) {
        res = 1;
    }
    if (!(p.contient_point(point(0.25,0.25)))) {
        res = 1;
    }
    return res;
}

int polygone::testu_2() {
    int res = 0;
    return res;
}

int polygone::testu_3() {
    int res = 0;
    return res;
}

int polygone::all_testsu() {
    int c = 0, t = 0; 
    polygone p;
    c = p.testu_1(); t += c;
    c = p.testu_2(); t += c;
    c = p.testu_3(); t += c;
    cout << " Tests unitaires réussis, polygone : " << 3-t << "/3" << endl;
    return t;
}

