#include <iostream>

#include "quadrangle.hpp"
#include "maillage.hpp"

int main() {
    // On crée un maillage triangle régulier
    maillage<triangle, 3> m;
    m.maillage_regulier(5);
    cout << m << endl;
    // On trace le maillage
    m.trace();
    return 0;
}
