#include <iostream>
#include <cmath>

#include "quadrangle.hpp"

using namespace std;

quadrangle::quadrangle() : polygone(4) {
}

quadrangle::quadrangle(point *p) : polygone(4, p) {
    set_h(); 
}

void quadrangle::set_h() {
    m_h = m_segments[0].longueur();
    if (m_h > m_segments[1].longueur()) {
        m_h = m_segments[1].longueur();
    }
    if (m_h > m_segments[2].longueur()) {
        m_h = m_segments[2].longueur();
    }
    if (m_h > m_segments[3].longueur()) {
        m_h = m_segments[3].longueur();
    }
}

void quadrangle::divise_triangles(triangle & t1, triangle & t2) {
    point liste_points[] = {m_sommets[0], m_sommets[1], m_sommets[2]};
    t1 = triangle(liste_points);
    liste_points[1] = m_sommets[2];
    liste_points[2] = m_sommets[3];
    t2 = triangle(liste_points);
}

int quadrangle::testu_1() { // Test constructeur, getter, setter
    int res = 0;
    point liste_points[] = {point(1,2), point(4,2), point(1,6), point(-1, 3)};
    quadrangle q = quadrangle(liste_points);
    if (q.getPoint(0) != point(1,2)) {
        res = 1;
    }
    if (q.getPoint(1) != point(4,2)) {
        res = 1;
    }
    if (q.getPoint(2) != point(1,6)) {
        res = 1;
    }
    if (q.getPoint(3) != point(-1,3)) {
        res = 1;
    }
    if (abs(q.get_h() - sqrt(5)) > 1e-8) {
        res = 1;
    }
    point p = point(1, 3);
    q.setPoint(0, p); q.setPoint(1, p); q.setPoint(2, p); q.setPoint(3, p);
    if (q.getPoint(0) != p) {
        res = 1;
    }
    if (q.getPoint(1) != p) {
        res = 1;
    }
    if (q.getPoint(2) != p) {
        res = 1;
    }
    if (q.getPoint(3) != p) {
        res = 1;
    }
    return res;
}

int quadrangle::testu_2() { // Test aire, barycentre
    int res = 0;
    point liste_points[] = {point(0, 0), point(1, -1), point(2, 0), point(1, 1)};
    quadrangle q = quadrangle(liste_points);
    if (abs(q.aire() - 2) > 1e-8) {
        res = 1;
    }
    if (q.barycentre() != point(1,0)) {
        res = 1;
    }
    return res;
}

int quadrangle::testu_3() { // Test contient_point, divise_triangles
    int res = 0;
    point liste_points[] = {point(0, 0), point(1, -1), point(2, 0), point(1, 1)};
    quadrangle q = quadrangle(liste_points);
    if (!(q.contient_point(point(1, 0.5)))) {
        res = 1;
    }
    if (q.contient_point(point(-1, 0))) {
        res = 1;
    }
    triangle t1, t2;
    q.divise_triangles(t1, t2);
    if ((t1.getPoint(0) != point(0, 0)) || (t1.getPoint(1) != point(1, -1)) || (t1.getPoint(2) != point(2, 0))) {
        res = 1;
}
    if ((t2.getPoint(0) != point(0, 0)) || (t2.getPoint(1) != point(2, 0)) || (t2.getPoint(2) != point(1, 1))) {
        res = 1;
}
         
    return res;
}

int quadrangle::all_testsu() {
    int c = 0, t = 0; 
    quadrangle q;
    c = q.testu_1(); t += c;
    c = q.testu_2(); t += c;
    c = q.testu_3(); t += c;
    cout << " Tests unitaires réussis, quadrangle : " << 3-t << "/3" << endl;
    return t;
}

