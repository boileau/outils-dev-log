#include <cmath>

#include "point.hpp"

using namespace std;

point::point() {
    m_x = 0; m_y = 0;
}

point::point(double abs, double ord) {
    m_x = abs; m_y = ord;
}

point::point(const point & p) {
    m_x = p.m_x; m_y = p.m_y;
}

point::~point() {
}

double point::get_x() {
    return m_x;
}

double point::get_y() {
    return m_y;
}

void point::set_x(double abs) {
    m_x = abs;
}

void point::set_y(double ord) {
    m_y = ord;
}

point point::operator +(point p) {
    point res;
    res.m_x = m_x + p.m_x;
    res.m_y = m_y + p.m_y;
    return res;
}

point point::operator *(double k) {
    point res;
    res.m_x = m_x * k;
    res.m_y = m_y * k;
    return res;
}

point point::operator -(point p) {
    point res;
    res.m_x = m_x - p.m_x;
    res.m_y = m_y - p.m_y;
    return res;
}

point & point::operator =(const point & p) {
    if (&p != this) {
        m_x = p.m_x;
        m_y = p.m_y;
    }
    return *this;
}

double point::operator ,(point p) {
    double res = p.m_x * m_x + p.m_y * m_y;
    return res;
}

double point::operator ^(point p) {
    double res;
    res = m_x * p.m_y - m_y * p.m_x;
    return res;
}

double point::norme() {
    double res = sqrt(m_x * m_x + m_y * m_y);
    return res;
}

bool point::operator <(point p) {
    bool res = false;
    if (this->norme() < p.norme()) {
        res = true;
    }
    return res;
}

bool point::operator >(point p) {
    bool res = false;
    if (this->norme() > p.norme()) {
        res = true;
    }
    return res;
}

bool point::operator ==(point p) {
    bool res = false;
    if ((abs(this->m_x - p.m_x) < 1e-8) && (abs(this->m_y - p.m_y) < 1e-8)) {
        res = true;
    }
    return res;
}

bool point::operator !=(point p) {
    bool res = false;
    if ((abs(this->m_x - p.m_x) > 1e-8) || (abs(this->m_y - p.m_y) > 1e-8)) {
        res = true;
    }
    return res;
}
        
std::ostream & operator <<(std::ostream & out, const point & p) {
    out << "(" << p.m_x << ", " << p.m_y << ")";
    return out;
}

// Test des constructeurs, setters, getters, opérateurs d'égalité,
// inégalité, affectation
int point::testu_1() {
    int res = 0;

    point p1 = point(3,-4);
    if ((abs(p1.m_x - 3) > 1e-8) || (abs(p1.m_y + 4) > 1e-8)) {
        res = 1;
    }
    point p;
    if ((abs(p.m_x) > 1e-8) || (abs(p.m_y) > 1e-8)) {
        res = 1;
    }

    p.set_x(10);
    p.set_y(56);
    if ((abs(p.get_x() - 10) > 1e-8) || (abs(p.get_y() - 56) > 1e-8)) {
        res = 1;
    }
    if (p != point(10, 56)) {
        res = 1;
    }
    if (!(p == point(10, 56))) {
        res = 1;
    }

    p = p1;
    if (p != p1) {
        res = 1;
    }
    return res;
}

int point::testu_2() { // Test des opérations
    int res = 0;
    point p1 = point(1,-6);
    point p2 = point(-3, 4);
    if (p1 + p2 != point(-2, -2)) {
        res = 1;
    }
    if (p1 - p2 != point(4, -10)) {
        res = 1;
    }
    if (abs((p1,p2) + 27) > 1e-8) {
        res = 1;
    }
    if (abs((p1^p2) + 14) > 1e-8) {
        res = 1;
    }
    if (abs(p1.norme() - sqrt(37)) > 1e-8) {
        res = 1;
    }
    double k = 0.5;
    if (p1 * k != point(0.5, -3)) {
        res = 1;
    }
    return res;
}

int point::testu_3() { // Test des comparaisons
    int res = 0;
    point p = point(1, 2);
    point q = point(3, -7);
    if (p > q) {
        res = 1;
    }
    if (!(p < q)) {
        res = 1;
    }
    return res;
}

int point::all_testsu() {
    int c = 0, t = 0; 
    point p;
    c = p.testu_1(); t += c;
    c = p.testu_2(); t += c;
    c = p.testu_3(); t += c;
    cout << " Tests unitaires réussis, point : " << 3-t << "/3" << endl;
    return t;
}
